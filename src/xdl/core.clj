(ns xdl.core
  (:gen-class)
  (:require
   [clojure.java.io :as io]
   [clojure.string :as str]
   [clojure.core.match :refer [match]]
   [clojure.core.match.regex]
   [clojure.tools.cli :refer [parse-opts]]
   [next.jdbc.result-set :as rs]
   [next.jdbc.sql :as sql]
   [next.jdbc :as jdbc]
   [silvur.datetime :refer [datetime datetime*] :as sd]
   [silvur.log :as log]
   [clojure.data.json :as json]
   )
  (:import
   (java.sql DriverManager)))

(def sample-oracle-spec {:dbtype "oracle" :host "172.18.1.32" :user "mule" :password "mule123" :dbname "FREEPDB1"})
(def sample-mysql-spec {:dbtype "mysql" :host "172.18.1.33" :user "mule" :password "mule123" :dbname "mule"})
(def sample-postgres-spec {:dbtype "postgresql" :host "172.18.1.35" :user "mule" :password "mule123" :dbname "mule"})


(defn get-product-name [c]
  (.. c getMetaData getDatabaseProductName))


(defn get-columns
  ([c tbl]
   (get-columns c nil tbl))
  ([c schema tbl]
   (rs/datafiable-result-set (.. c getMetaData (getColumns nil nil tbl nil)))))

(defn get-tables [c & [tbl-pattern]]
  (rs/datafiable-result-set (.. c getMetaData (getTables nil nil tbl-pattern (into-array ["TABLE"])))))

(defn gen-columns-schema-conv
  ([c tbl]
   (gen-columns-schema-conv c nil tbl))
  ([c schema tbl & [timestamp-formatter]]
   (->> (get-columns c schema tbl)
        (map (juxt :COLUMN_NAME :TYPE_NAME) )
        (reduce (fn [r [k v]]
                  (assoc r (keyword (str/lower-case k)) (match [(str/lower-case v)]
                                                               [#"int.*"] parse-long
                                                               [#"float"] parse-double
                                                               [#"number"] parse-long
                                                               [#"timestamp.*"] #(datetime % (or timestamp-formatter
                                                                                                 "yyyy-MM-dd'T'HH:mm:ss"))
                                                               :else identity)))
                {}))))

(defn flush-table [conn tbl]
  (sql/delete! conn tbl ["id > ?" -1]))

(defn lazy-load-csv [conn table file-path & {:keys [batch-row-size schema-fn-map header? separator]
                                             :or {batch-row-size 5000 separator "," header? true}}]
  (let [rdr (io/reader file-path)]
    (let [csv-lines (line-seq rdr)
          sep (re-pattern separator)
          headers-candidate (str/split (first csv-lines) sep)
          [headers lines] (if header?
                              [(map keyword headers-candidate) (rest csv-lines)]
                              [(map-indexed #(keyword (str (inc %1))) headers-candidate) csv-lines])]

      (log/debug "header:" (seq headers))
      
      (transduce (comp
                  (map #(str/split % sep))
                  (map (fn [coll]
                         (mapv (fn [h v]
                                 (if-let [f (h schema-fn-map)]
                                   [h (f v)]
                                   [h v]))
                               headers coll)))
                  (partition-all batch-row-size))
                 
                 
                 (completing
                  (fn
                    ([c vs]
                     (sql/insert-multi! conn table (map #(into {} %) vs) {:return-keys false})
                     (+ c (count vs)))))
                 0
                 lines))))

(def cli-options [["-h" "--host HOST" "Server host name or IP"]
                  ["-P" "--port PORT" "TCP Port"
                   :parse-fn parse-long]
                  ["-u" "--user USER" "User name"]
                  ["-p" "--password PASSWORD" "Password"]
                  ["-s" "--schema SCHEMA" "DB schema"]
                  ["-f" "--source-path PATH" "Source path"]
                  ["-x" "--timestamp-format FORMAT" "Java time format to parse timestamp (ex. 'yyyy-MM-dd'T'HH:mm:ss)"
                   :default "yyyy-MM-dd'T'HH:mm:ss"]
                  ["-d" "--debug" "Debug option"
                   :default false]
                  ["-F" "--separator CHAR" "Field separator"
                   :default ","]
                  ["-b" "--batch-row-size SIZE" "Batch size for loading"
                   :default 5000]
                  ["-o" "--output-format FORMAT" "Output format as edn or json "
                   :default :edn
                   :parse-fn keyword]
                  [nil "--help" "This Help"]])

(defn usage [summary]
  (->> ["Usage:"
        ""
        summary
        ""
        "Example:"
        ""
        "# Load from stdin"
        "  cat account.csv |  java -jar xdl.jar load oracle.FREEPDB1 ACCOUNT  -h 172.18.1.32  -u mule -p  mule123  -d -x 'yyyy-MM-dd HH:mm:ss'"
        ""
        "# Load from a file"
        "  java -jar xdl.jar load oracle.FREEPDB1 ACCOUNT -h 172.18.1.32  -u mule -p  mule123  -d -x 'yyyy-MM-dd HH:mm:ss' -f account.csv"        
        ""
        "# Execute SQL (use context 'oracle' in ~/.xdlrc)"
        "  java -jar xdl.jar sql oracle 'select count(*) from ACCOUNT'"                
        ""]
       (str/join \newline)
       (println)))



(defn load-context []
  (try
    (read-string (slurp (io/file (System/getenv "HOME") ".xdlrc")))
    (catch Exception e {})))

(defn -main [& args]
  (let [{:keys [options arguments summary errors]} (parse-opts
                                                    args
                                                    cli-options)]
    (if (or (:help options) (empty? arguments))
      (usage summary)
      (let [{:keys [debug]} options
            [sub-cmd db-or-ctx-name & others] arguments
            ctx (if-let [a-ctx ((load-context) (keyword db-or-ctx-name))]
                  (merge  a-ctx options)
                  (let [[_ dbtype dbname] (re-find #"(.*)[.:](.*)" db-or-ctx-name)]
                    (log/debug "DB: " dbtype dbname)
                    (assoc options :dbtype dbtype :dbname dbname )))]
        
        
        (log/set-level! (if debug :debug :info))
        (log/debug "context:" ctx)
        (assert (and (:dbtype ctx) (:dbname ctx)) "dbtype and/or dbname are not specified")
        (try
          (let [{:keys [schema timestamp-format source-path output-format]} ctx] 
            (log/debug "timestamp format:" timestamp-format)
            (cond-> (match arguments
                           ["load" _ tbl] (jdbc/on-connection [conn  (jdbc/get-datasource ctx)]
                                            (->> (gen-columns-schema-conv conn schema tbl timestamp-format)
                                                 (assoc ctx :schema-fn-map)
                                                 (lazy-load-csv conn tbl (or source-path *in*))))
                           ["sql" _ & stmt] (jdbc/on-connection [conn  (jdbc/get-datasource ctx)]
                                              (log/debug stmt)
                                              (jdbc/execute! conn stmt))
                           :else (throw (ex-info "Invalid arguments. Maybe table is not specifed."  {:args arguments})))
              (= :edn output-format) (clojure.pprint/pprint)
              (= :json output-format) (->> (map (fn [m] (into {} (mapv (fn [[k v]] [k (if (number? v) v (str v))]) m))))
                                           (json/pprint))))
          (catch Exception e (do (println (ex-message e))
                                 (log/debug (ex-cause e)))))))))
